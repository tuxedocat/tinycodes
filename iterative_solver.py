#!/usr/bin/env python
# coding: utf-8
"""
iterative_solver.py
Created on Wed Dec 04 13:03:53 2013
@author: Sawai Yu

線形方程式系の反復解法
（密行列として扱っているので大規模な問題には不向き）

Usage
-----
# Solverオブジェクト生成、引数でJacobi・Gauss-Seidelなど解法を指定
# 収束判定のための定数もここで指定
solver = IterativeSolver(method="Gauss-Seidel", eps=1e-8)

# 係数行列および右辺は行列形式で与える
A = numpy.array([[2.0,1.0],[5.0,7.0]])
b = numpy.array([11.0,13.0])

# 解の推定値はあってもなくてもよい
guess = numpy.array([1.0,1.0])

# 解はベクトルとして得られる
x = solver.solve(A, b, guess)
"""

from pprint import pprint
import numpy
import pylab


class IterativeSolver(object):
    def __init__(self, method=None, eps=1e-6):
        """
        Constructor args
        ----------------
        method: string, one of ["jacobi", "gauss-seidel", "SOR"]
        eps :   float,  criteria of convergence
        """        
        
        if method.lower() == "jacobi":
            self.solve = self.jacobi
        elif method.lower() == "gauss-seidel":
            self.solve = self.gauss_seidel
        elif method.lower() == "sor":
            raise NotImplementedError
        else:
            raise NotImplementedError("Method must be specified")
        
        if eps:
            self.epsilon = eps
        else:
            self.epsilon = 1e-6
        
        self.MAXITER = 1000
        self.residuals = []
        
    def jacobi(self, A, b, x=None):
        """
        Jacobi iterative method
        
        Args
        -----
        A: numpy ndarray (2d), left hand side of the linear system
        b: numpy ndarray (1d), right hand side of the linear system
        x: numpy ndarray, initial guess
        
        Returns
        -------
        x: roots of the linear system
        """
        # Create an initial guess if needed                                                                                                                                                            
        if x is None:
            x = numpy.zeros(A.shape[0])
    
        # A = diag(A) + R                                                                                                                                                                
        D = numpy.diag(A)
        R = A - numpy.diagflat(D)
        relres = 1
        nloop = 0
        tmpres = [] 
        # Iterate until it's converged
        # (D+R) x = b
        # x_new = (b - Rx)/D
        while relres > self.epsilon:
            nloop += 1
            x_old = x
            x = (b - numpy.dot(R,x)) / D
            relres = numpy.linalg.norm(x - x_old)
            tmpres.append(relres)
            
            if nloop >= self.MAXITER:
                break

        print "Converged at iteration %d: Residual norm"%nloop
        self.residuals.append(tmpres)
        pprint(relres)
        return x

    def gauss_seidel(self, A, b, x=None):
        """
        Gauss-Seidel iterative method
        
        Args
        -----
        A: numpy ndarray (2d), left hand side of the linear system
        b: numpy ndarray (1d), right hand side of the linear system
        x: numpy ndarray (1d), initial guess

        Returns
        -------
        x: roots of the linear system
        """
        # Create an initial guess if needed                                                                                                                                                            
        if x is None:
            x = numpy.zeros(A.shape[0])
    
        # Triangular decomposition (not proper LU)
        # Ax = b
        # (L+U)x = b
        # Lx = b - Ux
        L = numpy.tril(A)
        U = numpy.triu(A,1)
        L_i = numpy.linalg.inv(L)
        
        relres = 1
        nloop = 0
        tmpres = [] 
        # Iterate until it's converged
        # L x_new = b - U x_old
        while relres > self.epsilon:
            nloop += 1
            x_old = x
            x = numpy.dot(L_i, (b - numpy.dot(U, x_old)))
            relres = numpy.linalg.norm(x - x_old)
            tmpres.append(relres)

            if nloop >= self.MAXITER:
                break

        print "Converged at iteration %d: Residual norm"%nloop
        self.residuals.append(tmpres)
        pprint(relres)
        return x
    


if __name__=="__main__":
    A = numpy.array([[2.0,1.0],[5.0,7.0]])
    b = numpy.array([11.0,13.0])
    guess = numpy.array([1.0,1.0])
    
    solver_jacobi = IterativeSolver("jacobi")
    sol = solver_jacobi.solve(A, b, x=None)
        
    print "A:"
    pprint(A)
    
    print "b:"
    pprint(b)
    
    print "x:"
    pprint(sol)
    
    solver_gauss_seidel = IterativeSolver("gauss-seidel")
    sol = solver_gauss_seidel.solve(A, b, x=None)
        
    print "A:"
    pprint(A)
    
    print "b:"
    pprint(b)
    
    print "x:"
    pprint(sol)
    
    solver_SOR = IterativeSolver("SOR")
    sol = solver_gauss_seidel.solve(A, b, x=None)
