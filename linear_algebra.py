#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
LinAlg
線形代数のお勉強
※LU分解編

"""
import pprint
import numpy as np


class BaseLinAlg(object):
    @classmethod
    def _notNone(cls, mat):
        if mat is None:
            raise TypeError
    
    @classmethod
    def _isSquare(cls, mat):
        assert mat.shape[0] == mat.shape[1]
    
    @classmethod
    def lu_decomposition(cls, mat=None):
        raise NotImplementedError

    @classmethod
    def det(cls, mat=None):
        raise NotImplementedError
        
    @classmethod
    def eigenvalues(cls, mat=None):
        raise NotImplementedError

    @classmethod
    def linsolve(cls, mat=None):
        raise NotImplementedError
        
    @classmethod
    def test(cls):
        A = np.matrix([[2.0,6.0,4.0], [5.0,7.0,9.0],[7.0,8.0,1.0]])
        assert cls.det(A) == 182.0
    
    @classmethod
    def test2(cls):
        A = np.matrix([[2.0,3.0,3.0], [3.0,4.0,2.0],[-2.0,-2.0,3.0]])
        y = np.matrix(np.array([9.0,9.0,2.0])).T
        x = cls.linsolve(A,y)
#        np.testing.assert_array_equal(x, np.linalg.solve(A,y))
#        print "Test passed: linear system solve 1"

"""
----------------------------
Implementation of BaseLinAlg
----------------------------
"""
class LinAlg(BaseLinAlg):
    def __init__(self):
        pass

    @classmethod
    def lu_decomposition(cls, mat=None):
        """
        プログラミングのための線形代数 p176を愚直に実装したもの
        """
        cls._notNone(mat)
        L = np.matrix(np.zeros(mat.shape))
        U = np.matrix(np.zeros(mat.shape))
        A = mat.copy()
        nrow, ncol = mat.shape
        s = nrow if nrow < ncol else ncol

        for k in range(s):
            print "k = %d"%k
            # l(k): m x 1 列ベクトル
            # l(k) = vec_0 + 1/A_kk * A[k:s, k::]
            # np.r_ で列ベクトルを結合
            L[...,k] = np.r_[np.matrix(np.zeros((k,1))), 1.0/A[k,k] * A[...,k][k:]]
            pprint.pprint(L[...,k])
            
            # u(k): 1 x n 行ベクトル
            # u(k) = vec_0 + A[k, k::]
            # np.c_は行ベクトルの結合
            U[k,...] = np.c_[np.matrix(np.zeros((1,k))), A[k,k:]]
            pprint.pprint(U[k,...])

            # Aを残差の行列としているので，最終的にはゼロ行列にならないといけない
            A -= L[...,k] * U[k,...]
            print "L:"
            pprint.pprint(L)
            print "U:"
            pprint.pprint(U)
            print "A:"
            pprint.pprint(A)
        return (L,U)

    @classmethod
    def det(cls, mat=None):
        cls._isSquare(mat)
        L, U = cls.lu_decomposition(mat)
        x = 1
        for u in np.nditer(U.diagonal()):
            x *= u
        return x
        
    @classmethod
    def linsolve(cls, mat, y):
        """
        Solve linear system
        
        Parameters
        ----------
        mat: coefficient matrix
        y  : right hand side column vector (np.matrix)
        """
        cls._notNone(mat)
        assert mat.shape[1] == y.shape[0]        
        L, U = cls.lu_decomposition(mat)
        
        # 1st part:
        # solving L*z = y
        print "L * z = y"
        z = np.matrix(np.zeros((y.shape[0], 1)))
        for i in range(y.shape[0]):
            print i
            z[i,0] = y[i,0] - sum((L[i,j]*z[j,0] for j in range(i+1)))
            pprint.pprint(z)
        
        # 2nd part:
        # solving U*x = z
        print "U * x = z"
        x = np.matrix(np.zeros((y.shape[0],1)))
        for i in np.arange(y.shape[0]-1, -1, -1):
            print i
            x[i,0] = z[i,0] - sum((U[i,j]*x[j,0] for j in range(i+1, y.shape[0])))
            x[i,0] = x[i,0] / U[i,i]
            pprint.pprint(x)


class LinAlg_ref(BaseLinAlg):
    @classmethod
    def lu_decomposition(cls, mat=None):
        """
        Parameter
        ================
        mat: numpy matrix
        """
        cls._notNone(mat)
        # Initialize L, U matrices
        L = np.matrix(np.zeros(mat.shape))
        U = np.matrix(np.zeros(mat.shape))

        nrow, ncol = mat.shape
        # No pivotting, just proceed...
        s = nrow if nrow < ncol else ncol
        for k in range(s):
            L[k,k] = 1.0    # Diagonal val. of L is 1

            for i in range(k+1):
                s1 = sum(U[j,k] * L[i,j] for j in range(i))
                U[i,k] = mat[i,k] - s1

            for i in range(k, s):
                s2 = sum(U[j,k] * L[i,j] for j in range(k))
                L[i,k] = (mat[i,k] - s2) / U[k,k]

        return (L, U)


if __name__ == "__main__":
    LinAlg.test()
    LinAlg.test2()

