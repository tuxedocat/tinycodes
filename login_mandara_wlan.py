#! /usr/bin/env python
# coding: utf-8
"""
login_mandara_wlan.py
"""
__author__ = "Yu Sawai"
__copyright__ = "Copyright 2012, Yu Sawai"
__version__ = "0.00001"
__status__ = "Beta"


import mechanize
from mechanize import HTTPError
import keyring


def login(username="", keyname=""):
    try:
        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.open('https://aruba.naist.jp/cgi-bin/login?cmd=login')
        br.select_form(nr=0)
        if username and keyname:
            br['user'] = username
            br['password'] = keyring.get_password(keyname, username)
        else:
            raise NameError
        br.submit()
        print "Successfully done"
    except HTTPError, he:
        if he.code == 302:
            print "Seems you're already logged in."
        else:
            print "Seems this script is not accepted by the server... Terminated."
    except NameError:
        print "Use Keychain for secure access!!!"
    finally:
        br.close()


if __name__ == '__main__':
    import sys
    import argparse
    import os
    import yaml
    argv = sys.argv
    argc = len(argv)
    description = """python login_mandara_wlan.py ( |-s <path_to_setting.yaml>)\n\nconfig file is like \n\t'user: <username>\n\tkeychain: <keychainname>'"""
    ap = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    ap.add_argument("-s", "--setting_file", action="store", nargs="?",
                    help="path to setting file (written in yaml, if not given use ~/naistwlan.yaml instead)")
    args = ap.parse_args()

    if args.setting_file:
        s_file = args.setting_file
    else:
        s_file = os.path.join(os.environ['HOME'], "naistwlan.yaml")
    print "Using config file %s" % s_file
    with open(s_file, "r") as f:
        conf = yaml.load(f.read())
    login(conf["user"], conf["keychain"])
