#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 04 17:03:53 2013

@author: Sawai Yu

二次元非定常熱伝導問題を陰解法で解くコード
"""
import os,sys
import numpy, scipy
from pprint import pprint
from iterative_solver import IterativeSolver
import pylab
import time


class HeatTransferProblem(object):
    def __init__(self, x, y, dx, dt, lmd, c, rho):
        numpy.set_printoptions(linewidth=200)
        # 離散化後の線形方程式系
        self.eqnmat = numpy.zeros((x*y, x*y))
        self.eqnrhs = numpy.zeros(x*y)
        
        # x*y の構造格子を表す行列
        self.raw_x = x
        self.raw_y = y
        self.mesh = numpy.zeros((x,y))
        
        # 空間刻み・時間刻み
        self.dx = dx
        self.dt = dt
        
        # 時間刻みと特性拡散時間の比
        # コンピュータによる流体力学 p.139
        self.d = (lmd * dt) / (c * rho * dx**2)
       
    def set_eqn(self):
        """
        離散化した係数行列を決定
        """
        i, j = numpy.indices(self.eqnmat.shape)
        # For cells of Tp
        self.eqnmat[i==j] += (1 + 4.0 * self.d)
        
        # For cells of Tw,Ts,Tn,Te
        for _i in i[:,0]:
            for _j in j[0]:
                if _i==_j+1 and _i%(self.raw_x)!=0:
                    self.eqnmat[_i, _j] -= self.d
                if _i==_j-1 and _j%(self.raw_x)!=0:
                    self.eqnmat[_i, _j] -= self.d
                if _i==_j+self.raw_x:
                    self.eqnmat[_i, _j] -= self.d                
                if _i==_j-self.raw_x:
                    self.eqnmat[_i, _j] -= self.d

        return self.eqnmat
    

    def set_initial(self, init=numpy.array([()])):
        """
        初期条件を設定
        
        Args
        ----
        init: numpy ndarray, (x,y,T)のList
        """
        self.init = numpy.zeros(self.mesh.shape)
        ix = numpy.array([t[0] for t in init])
        iy = numpy.array([t[1] for t in init])
        T = numpy.array([t[2] for t in init])
        self.init[ix,iy] = T


    def set_bc(self, bc_dirichlet=None, bc_neumann=None):
        """
        境界条件を設定
        
        Args
        ----
        bc_dirichlet: Dirichlet境界条件, (x, y, value)を要素とするList
        bc_neumann:   Neumann境界条件 （現状は断熱のみ）, (x, y, 0)を要素とするList
        """
        # Dirichlet境界条件
        _bcd = numpy.zeros(self.mesh.shape)        
        if bc_dirichlet:
            ix_bcd = numpy.array([t[0] for t in bc_dirichlet])
            iy_bcd = numpy.array([t[1] for t in bc_dirichlet])
            val_bcd = numpy.array([t[2] for t in bc_dirichlet])
            
            # Dirichlet境界条件の設定: 
            _bcd[ix_bcd, iy_bcd] = val_bcd * self.d
            self.eqnrhs += _bcd.flatten("F")

        # Neumann境界条件 （壁面の断熱のみ）        
        _bcn = numpy.zeros(self.mesh.shape)
        if bc_neumann:           
            for t in bc_neumann:
                _bcn[t[0], t[1]] -= t[2] * self.d
            
            i, j = numpy.indices(self.eqnmat.shape)
            self.eqnmat[i==j] += _bcn.flatten("F")
            print "\n\nNeumann"
            pprint(_bcn)            
            

    def update(self, newrhs):
        """
        Update right hand side with solved values
        """
        print "Before"
        pprint(self.eqnrhs)
        print "Updated"
        pprint(newrhs)
        self.eqnrhs = newrhs

       

def solvetest(endtime=1000):
    pylab.ion()
    pylab.figure("Result")
    pylab.show()
    pylab.figure("Residuals")
    pylab.show()
    
    # 解析領域は矩形限定
    x,y = (5,5)
    dx = 0.01
    dt = 0.001
    lmd = 260
    c = 10
    rho = 1000
    endtime = endtime if endtime else 100

    # Dirichlet境界条件（定数） ※セル添字は0始まり
    bc_d = [(0,0,5), (4,4,10)]
    
    # Neumann境界条件（断熱） ※セル添字は0始まり    
    bc_n = [(0,0,1),(1,0,1),(2,0,1),(3,0,1),(4,0,2),
            (0,1,1),(0,2,1),(0,3,1),(0,4,2),
            (1,4,1),(2,4,1),(3,4,1),(4,4,1),
            (4,1,1),(4,2,1),(4,3,1)]

    # 行列形の微分方程式の設定
    p = HeatTransferProblem(x=x, y=y, dx=dx, dt=dt,
                            lmd=lmd,c=c,rho=rho)
    p.set_eqn()
    p.set_bc(bc_neumann=bc_n)

    # 初期値・初期解の生成
    init = numpy.zeros(p.eqnrhs.shape)
    solver = IterativeSolver("gauss-seidel", eps=1e-6) 
    p.eqnrhs = solver.solve(p.eqnmat, init)
    pylab.figure("Result")
    img = pylab.imshow(p.eqnrhs.reshape(x, y, order='F'), interpolation='nearest',
                            animated=True, label="Result", vmin=0, vmax=10)
    pylab.colorbar()
    
    # Outer Loop
    try:
        for t in range(endtime):
            p.set_bc(bc_dirichlet=bc_d)
            print "\n\n---------Time %3.6f----------"%((t+1)*dt)
            
            # Iterative Solver loop
            new = solver.solve(p.eqnmat, p.eqnrhs)
            p.update(new)            
            
            pylab.figure("Result")
            img.set_data(new.reshape(x, y ,order='F'))
            pylab.draw()
            #pylab.savefig("./img/TempField_%d.png"%t, format="png")


    except KeyboardInterrupt:
        pass

    finally:    
        print "\n"+"="*80+"\nDiscreted Matrix\n"+"="*80
        pprint(p.eqnmat)
        print "\n"+"="*80+"\nInitial Condition\n"+"="*80
        pprint(init.reshape(5,5,order="F"))
        print "\n"+"="*80+"\nSolved Tempreture Field\n"+"="*80    
        pprint(p.eqnrhs.reshape(5,5,order="F"))

        pylab.figure("Residuals")
        residuals_p = []
        residuals_steps = []
        for r in solver.residuals:
            residuals_steps.append(len(r))
            residuals_p.extend(r)
        pylab.yscale("log")
        pylab.plot([i for i in range(len(residuals_p))],residuals_p)
        pylab.xlabel("Iteration (Inner)")
        pylab.ylabel("Residuals")
        pylab.draw()
        return p.eqnrhs.reshape(5,5,order="F")


def probtest():
    p = HeatTransferProblem(5,5,0.01,0.01,260,10,1000)
    p.d = 1.0
    r = p.set_eqn()
    bc_d = [(0,0,5), (4,4,10)]
    bc_n = [(0,0,1),(1,0,1),(2,0,1),(3,0,1),(4,0,2),
            (0,1,1),(0,2,1),(0,3,1),(0,4,2),
            (1,4,1),(2,4,1),(3,4,1),(4,4,1),
            (4,1,1),(4,2,1),(4,3,1)]
    init = numpy.zeros(p.eqnrhs.shape)
    p.set_bc(bc_dirichlet=bc_d, bc_neumann=bc_n)
    print "\n\nInitial"
    print p.eqnrhs.reshape(5,5,order='F')
    print "\n\nMatrix"
    print p.eqnmat


    
if __name__=="__main__":
    r = solvetest(10000)
