#! /usr/bin/env python
import simplejson
import urllib
import sys
import re
import time

"""
Download ted talks' transcriptions into current directory

USAGE
$ python dl_tedsubs_en.py <talkID_begin> <talkID_end>
"""

def dlrawsubtext(talkid, langcode):
    try:
        raw = simplejson.load(urllib.urlopen('http://www.ted.com/talks/subtitles/id/%s/lang/%s'%(talkid, langcode)))
        raw = raw['captions']
        with open('subs_%s_%s.txt'%(talkid,langcode), 'w') as txt:
            for line in raw:
                txt.write("%s "%(line['content'].encode('utf-8')))
    except:
        pass
    return 0


def getVideoParameters(urldirection):
    ht = urllib.urlopen(urldirection).read()
    var = re.search('flashVars = {\n([^}]+)}', ht)
    if var:
        var = var.group(1)
    else:
        return None
    var = [a.replace('\t', '') for a in var.split('\n')]
    for a in range(len(var)):
        if var[a]:
            var[a] = var[a][:var[a].rfind(',')]
    resultado = []
    for a in var:
        l = a.find(':')
        if l != -1:
            resultado.append((a[:l], a[l+1:]))
    return dict(resultado)


def main(ids,ide):
    for id in range(ids,ide+1):
        vidpar = getVideoParameters('http://www.ted.com/talks/view/id/%s'%id)
        if not vidpar:
            print("There was a problem fetching information about that TED Talk")
            id += 1
        dlrawsubtext(id, 'en')
        time.sleep(1)
    return 0


if __name__=="__main__":
    try:
        if len(sys.argv) < 3:
            print('Need more arguments!')
        else:
            main(int(sys.argv[1]),int(sys.argv[2]))
    except TypeError:
        print "Arguments must be integers"